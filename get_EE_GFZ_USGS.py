###################################################
#                                                 #
#  programmed on March 8, 2017 by Franco Mele     #
#                                                 #
###################################################

#---------------------------------------------------------------------
#  installare:                                    
#     feedparser                                  
#     urllib 
#     numpy
#     obspy
#     Pillow
#     pywin32
#     beautifulsoup4
#     python-setuptools
#     python-virtualenv
#
#    Programmed by Franco Mele (first version: March 2017) 
#
#
#  2017-03-25:  aggiungo lo spawn "&" al comando "say" 
#  2017-03-25:  escludo la limitazione dulla magnitudo nel selezionare
#               gli ebventi da mostrare in EE
#  2018-11-20:  La prima versione e' stata realizzata per EE1.1.9
#               Sta per entrare in sala sismica ufficialmente 
#               a la versione EE1.2.3
#               che, nella pagina che fa la lista dei terremoti, mostra
#               due colonne in piu' (la prima credo sia il numero di
#               versione della localizzazione. La seconda colonna aggiunta
#               indica la qualita' della localizzaizone che puo' essere
#               A, B, C o D in ordine decrescente di bonta').
#               Percio' includo due parametri al lancio del programma:
#               il parametro  -v <versione> che puo' essere solo
#               1.1 o 1.2, e -u <ee_url> che specifica l'url completo
#               della lista delle localizzazioni effettuate da Early-est.
#---------------------------------------------------------------------

import feedparser
import sys, getopt
import tkinter as tk
import time 
from datetime import datetime
import urllib, os
from obspy import UTCDateTime
from PIL import Image, ImageTk
import webbrowser
import platform
import json
# import win32com.client as wincl


from bs4 import BeautifulSoup
import re

global eetx1, eetx2


#print len(feedparser.parse("http://geofon.gfz-potsdam.de/eqinfo/list.php?fmt=rss")["entries"])
k=1
j=1
cc = 0.0

stop_alarm = 0

title=[]
summary=[]
old_ids=[]
old_geos=[]
url_old=[]
txt1=[]
txt2=[]

eetx1=[]
eetx2=[]
ee_id=[]
ee_id_old=[]

#definisco note musicali
A = 440 # Set Frequency To 2500 Hertz
C = 523
Dsh = 622
Fsh = 740
A2 = 880 # Set Frequency To 2500 Hertz
C2 = 1046
Dsh2 = 1245
Fsh2 = 1480

Dur = 200 # Set Duration To 1000 ms == 1 second

def beep_eq():
    global stop_alarm
#    stop_alarm = 0    # dovrebbe essere posto a 1 da un click su un tasto
#    ic = 0
    #  platform.system() == 'Windows'
    if ( os.name == 'nt'):
        try:
        # provo a vedere se e- sistema windows
#            while ( stop_alarm == 0  and ic < 50): 
            speak = wincl.Dispatch("SAPI.SpVoice")
            speak.Speak(" Attention please!  new earthquake, new earthquake")
            speak.Speak(" Attention please!  new earthquake, new earthquake")

#                ic = ic + 1
        except Exception:
            pass

    elif ( platform.system() == 'Darwin' or platform.system() == 'darwin' ):         
        try:
        # nel caso di un OS X
#            while ( stop_alarm == 0  and ic < 50): 
                os.system('say beep')
                os.system('say -v Alice "attenzione terremoto"')
                os.system('say beep')
                os.system('say  -v Luca "terremoto terremoto terremoto"')
        except Exception:
            pass

    elif ( os.name == 'posix'):
        try:
        # vediamo se e- linux           
            os.system("espeak  'alert ! attention please ! new  earthquakes,  terremato ! terremato ! ' & ")
        except Exception:
            pass       

def hello(event):
#   global stop_alarm
    stop_alarm = 1
        
def quit0(event):                           
#    print("Double Click, so let's stop") 
    sys.exit()

def callback(event):
    webbrowser.open_new(event.widget.cget("text")) 
    
def callback2(event):
    webbrowser.open_new(event.widget.cget("text")) 
    
def callback3(event):
    webbrowser.open_new(event.widget.cget("text")) 
    
    

def best_m(mb, mwp, mwpd,  nmb, nmwp, nmwpd):
#
#      best     num fasi min.    Mag range
#      Mwpd      >=6             Mwp >=7.2
#      Mwp       >=6          5.8 <= Mwp < 7.2
#      Mb        >=6             Mwp < 5.8
#   
#    print(" in best_m   ", mb, mwp, mwpd,  nmb, nmwp, nmwpd)
    r_mb = mb
    if( int(nmb) < int("6")):
        r_mb = "-9.0"
    r_mwp = mwp
    if( int(nmwp) < int("6")):
        r_mwp = "-9.0"
    r_mwpd = mwpd
    if( int(nmwpd) < int("6")):
        r_mwpd = "-9.0"
    
    guess = r_mb
    if( "5.8" <= r_mwp and r_mwp < "7.2" ):
        guess = r_mwp
    if( r_mwp >= "7.2" and r_mwpd > r_mwp):
        guess = r_mwpd
    return guess

    
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #    
def get_EE_eqks():
    global EE, eetx1, eetx2, ee_id, ee_id_old
    EE = True  
#    eeurl="http://early-est.int.ingv.it/hypolist.html"
#    eeurl="http://early-est-dev.int.ingv.it:8080/hypolist.html"
    try:
        conn = urllib.request.urlopen(eeurl)
        #print (" EE connessione riuscita ")
        #print (conn)
    except Exception:
        EE = False 
        #print (" problemi con EE")
        pass
    
    #print(EE," 1")  
    if(EE):
        try:
            dataee = conn.read()
            #print(' new call to INGV ')
            #cc = (cc+1)%10
        except Exception: 
            #print(" EE lettura fallita")
            EE = False            
            
        if(EE):
            try:       
                with open('EE_data.txt','wb') as localFile:
                        localFile.write(dataee)
                        localFile.close()
            except Exception: 
                EE = False                
        
            with open('EE_data.txt') as localFile:
                dataee=localFile.read()
                localFile.close()    
# # elimino l'errore della doppia terminazione di campo.
            dataee = re.sub( "</td></td>","</td>" ,dataee )
            soup = BeautifulSoup(dataee, 'html.parser')

            g_data = soup.findAll('tr') 
            global luogo,mydata,lat,lon,dep,qual
            kev=0  
            eetx1=[]
            eetx2=[]       
            ee_id=[]
            lx = 0
            for tr in g_data:       # # loop sugli eventi
                lx = lx +1
                e_data = tr.findAll('td')     
                m=1 
                ev=[]   # # deve contenere gli elementi di un solo evento
                for td in e_data:   # #  loop sugli elementi di un evento
                    ev.append(td.string)
                    if (m==1):
                        idnow = td.string
                        ee_id.append(idnow) # memorizzo gli id
                        kk=re.search('page(.*)"',str(td))
                     # #kk=re.match(r'(?<={}).*?(?={})'.format("page", "\""), td)
                        luogo=kk.group(1)
# # #                     print (luogo)
                    elif (m==9):
                        mys=re.sub("-"," ",td.string)
                        mys=re.sub("\.","-",mys)
# # #                     print(mys)
                        mydata=mys
                    elif (m==12 or m==28 or m==29):
                        pass
                    elif(m==10 ):
                        lat=td.string
                    elif(m==11 ):
                        lon=td.string
                    elif(m==13 ):
                        dep=td.string+" km"                
                    elif(m==20 ):
                        mb=td.string
                        mb=mb.replace("\xa0","")
                    elif(m==21 ):
                        nmb=td.string
                        nmb=nmb.replace("\xa0","")
                    elif(m==22 ):
                        mwp=td.string
                        mwp=mwp.replace("\xa0","")
                    elif(m==23 ):
                        nmwp=td.string
                        nmwp=nmwp.replace("\xa0","")
                    elif(m==26 ):
                        mwpd=td.string
                        mwpd=mwpd.replace("\xa0","")
                    elif(m==27 ):
                        nmwpd=td.string
                        nmwpd=nmwpd.replace("\xa0","")                 
                    else: 
                        pass               
                    m = m + 1
                    
                if (len(ev) > 26 ):
#                    mx = max(mb,mwp,mwpd)                    
                    mx = best_m(mb, mwp, mwpd,  nmb, nmwp, nmwpd)

#                    if(mx > "4.9" and (idnow not in ee_id_old) and k > 1):
                    if( (idnow not in ee_id_old) and k > 1):
                        beep_eq()
                    if (mx=="-9.0"):
                        mx = "N.D"
                    stringa1="INGVe Mx"+mx+" "+luogo[2:]
                    eetx1.append(stringa1)        
                    stringa2 =  mydata+" \t"+lat+" \t"+lon+" \t"+dep+" "
                    stringa3=mb+"("+nmb+")"+ \
                        mwp+"("+nmwp+")"+ \
                        mwpd+"("+nmwpd+")"
                    stringa3=stringa3.replace(" ","")               
                    eetx2.append(stringa2+" \t"+stringa3)
                    #print(stringa1+" "+stringa2+" "+stringa3 )                   
                    #print(eetx1[kev]+" "+eetx2[kev])       
                        
            # # memorizzo gli id di evento ee trovati a questo giro.
            ee_id_old=[]
            for jj in ee_id :
                ee_id_old.append(jj)
    else:
        EE = False
 # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #    
    
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #    
def get_EE_eqks_new():
    global EE, eetx1, eetx2, ee_id, ee_id_old
    EE = True  
#    eeurl="http://early-est.int.ingv.it/hypolist.html"
#    eeurl="http://early-est-dev.int.ingv.it:8080/hypolist.html"
    try:
        conn = urllib.request.urlopen(eeurl)
        #print (" EE connessione riuscita ")
        #print (conn)
    except Exception:
        EE = False 
        #print (" problemi con EE")
        pass
    
    #print(EE," 1")  
    if(EE):
        try:
            dataee = conn.read()
            #print(' new call to INGV ')
            #cc = (cc+1)%10
        except Exception: 
            #print(" EE lettura fallita")
            EE = False            
            
        if(EE):
            try:       
                with open('EE_data.txt','wb') as localFile:
                        localFile.write(dataee)
                        localFile.close()
            except Exception: 
                EE = False                
        
            with open('EE_data.txt') as localFile:
                dataee=localFile.read()
                localFile.close()    
# # elimino l'errore della doppia terminazione di campo.
            dataee = re.sub( "</td></td>","</td>" ,dataee )
            soup = BeautifulSoup(dataee, 'html.parser')

            g_data = soup.findAll('tr') 
            global luogo,mydata,lat,lon,dep,qual
            kev=0  
            eetx1=[]
            eetx2=[]       
            ee_id=[]
          
            for tr in g_data:       # # loop sugli eventi
                e_data = tr.findAll('td')     
                m=1 
                ev=[]   # # deve contenere gli elementi di un solo evento
                for td in e_data:   # #  loop sugli elementi di un evento
                    ev.append(td.string)
                    if (m==1):
                        idnow = td.string
                        ee_id.append(idnow) # memorizzo gli id
                        kk=re.search('page(.*)"',str(td))
                     # #kk=re.match(r'(?<={}).*?(?={})'.format("page", "\""), td)
                        luogo=kk.group(1)
# # #                     print (luogo)
                    elif (m==10):
                        mys=re.sub("-"," ",td.string)
                        mys=re.sub("\.","-",mys)
# # #                     print(mys)
                        mydata=mys
                    elif (m==13 or m==30 or m==31):
                        pass
                    elif(m==11 ):
                        lat=td.string
                    elif(m==12 ):
                        lon=td.string
                    elif(m==14 ):
                        dep=td.string+" km" 
                    elif(m==16 ):
                        qual=td.string                                    
                    elif(m==22 ):
                        mb=td.string
                        mb=mb.replace("\xa0","")
                    elif(m==23 ):
                        nmb=td.string
                        nmb=nmb.replace("\xa0","")
                    elif(m==24 ):
                        mwp=td.string
                        mwp=mwp.replace("\xa0","")
                    elif(m==25 ):
                        nmwp=td.string
                        nmwp=nmwp.replace("\xa0","")
                    elif(m==28 ):
                        mwpd=td.string
                        mwpd=mwpd.replace("\xa0","")
                    elif(m==29 ):
                        nmwpd=td.string
                        nmwpd=nmwpd.replace("\xa0","")                 
                    else: 
                        pass               
                    m = m + 1
                    
                if (len(ev) > 28 ):
#                    mx = max(mb,mwp,mwpd)                    
                    mx = best_m(mb, mwp, mwpd,  nmb, nmwp, nmwpd)

#                    if(mx > "4.9" and (idnow not in ee_id_old) and k > 1):
                    if( (idnow not in ee_id_old) and k > 1):
                        beep_eq()
                    if (mx=="-9.0"):
                        mx = "N.D"
                    stringa1="INGVe Mx"+mx+" "+luogo[2:]
                    eetx1.append(stringa1)        
                    stringa2 =  mydata+" \t"+lat+" \t"+lon+" \t"+dep+" "+qual
                    stringa3=mb+"("+nmb+")"+ \
                        mwp+"("+nmwp+")"+ \
                        mwpd+"("+nmwpd+")"
                    stringa3=stringa3.replace(" ","")               
                    eetx2.append(stringa2+" \t"+stringa3)
                    #print(stringa1+" "+stringa2+" "+stringa3 )                   
                    #print(eetx1[kev]+" "+eetx2[kev])       
                        
            # # memorizzo gli id di evento ee trovati a questo giro.
            ee_id_old=[]
            for jj in ee_id :
                ee_id_old.append(jj)
    else:
        EE = False
 # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #    
            
    
class App():
    
    def __init__(self):
        self.root = tk.Tk()
        self.tempo = tk.Label(text="")
        self.tempo.pack()  
              
        self.widget = tk.Button(None, text='Events from INGVee, GFZ and USGS - double-click to close       (images by GFZ only)')
        self.widget.pack(side = 'top')
        self.widget.bind('<Button-1>', hello)
        self.widget.bind('<Double-1>', quit0)
        
        self.lbl3 = tk.Label(self.root, text=eeurl, fg="blue", cursor="hand2")
        self.lbl3.pack(side ='bottom')
        self.lbl3.bind("<Button-1>", callback3)             
        
        self.lbl2 = tk.Label(self.root, text=r"https://earthquake.usgs.gov/earthquakes/", fg="blue", cursor="hand2")
        self.lbl2.pack(side ='bottom')
        self.lbl2.bind("<Button-1>", callback2)             
        
        self.lbl = tk.Label(self.root, text=r"http://geofon.gfz-potsdam.de/eqinfo/list.php", fg="blue", cursor="hand2")
        self.lbl.pack(side ='bottom')
        self.lbl.bind("<Button-1>", callback)         
        
        img = Image.open("./desktop.jpg")
        photo=ImageTk.PhotoImage(img) 
        img.close()            
        self.panel = tk.Label(self.root, image = photo)
        self.panel.pack(side = 'right', fill = "both", expand = "yes")
                         
        self.T1 = tk.Text(self.root, height=20, width=130)
        self.T1.pack(side='right')   

        self.T1.tag_configure('gfz_c', foreground='darkgreen',font='monospace 9 bold')   #, font='Courier 10 bold italic')
        self.T1.tag_configure('usgs_c', foreground='blue',font='monospace 9 bold')       #,font='Courier 10 bold italic') 
        self.T1.tag_configure('ingv_c', foreground='red',font='monospace 9 bold')        #,font='Courier 10 bold italic') 
                        
        self.update_clock()
        self.root.mainloop()
       

    def update_clock(self):
        global k, j, old_ids, url, url_old, old_geos, img_name, cc, img_name_old

     
        # suppose success with GFZ
        GFZ = True
        try:
            feed = feedparser.parse("http://geofon.gfz-potsdam.de/eqinfo/list.php?fmt=rss").entries
            #print(' new call to GFZ ',cc)
            cc = (cc+1)%10
#          feed = feedparser.parse("http://early-est.int.ingv.it/monitor.xml").entries
        except Exception: 
            GFZ = False
            #self.root.after(60000, self.update_clock)
            pass  #se non trovo dati da GFZ provo con USGS

       
        if (k==1):
            img_name_old = "./desktop.jpg"
            img_name = "./desktop.jpg"
        
        if (GFZ):
        # preparo l'immagine da GFZ
            try:
                url =feed[0]['links'][1]['href']
            except Exception:
                GFZ= False

            if(GFZ):              
                if ( k == 1 ):
                    url_old = url  # devo ignorare url_old la prima volta
            # ma devo definirlo qui per non avere errore.
                if (not (url == url_old) or ( k == 1) ):
                    try:
                        imgg = urllib.request.urlopen(url)
                        newimag = True
                    except Exception: 
                        newimag = False  
                        pass
                
                    if(newimag):
                        try:
                            data = imgg.read()
                        except Exception:
                            newimag = False 
                            pass                                 
                    
                    if (newimag):  
                        with open('desktop.jpg','wb') as localFile:
                            localFile.write(data)
                            localFile.close()
                        img_name = "./desktop.jpg"
                    else:
                        img_name = "./image-not-available.jpg"           
            url_old = url
        else:
            img_name = "./image-not-available.jpg"
        img = Image.open(img_name)
        photo=ImageTk.PhotoImage(img)             
        img.close()
        self.panel.configure(image = photo)
        self.panel.image = photo
        img_name_old = img_name
        
        
        if(GFZ):
            j=0   # contatore delle righe da scrivere
            txt1=[]
            txt2=[]
            for i in feed:     
                txt1.append("GFZ   "+i['title'][0:5]+"  "+i['title'][7:])

                strstr = i['summary']
                while '  ' in strstr:
                    strstr = strstr.replace('  ',' ')
                i['summary']=strstr
                
                mylist = []
                mylist = i['summary'].split(" ")
            #    print(" summary ", mylist)
            #    print(mylist[0])
            #                date          time            lat             lon             dep           auto, conf or man
                txt2.append(mylist[0]+" "+mylist[1]+" \t"+mylist[2]+" \t"+mylist[3]+" \t"+mylist[4]+" km "+mylist[6]   )


            # estraggo la magnitudo
                mylist = []
                mylist = i['title'].split(" ")
                # print (" title " , mylist)
                mylist[1].encode('ascii','ignore')
                mylist[1].replace(","," ")
                mag = mylist[1][0:3]           
                if (mag > "4.9" and not (i['id'] in old_ids) and not ( k == 1 ) ):
                    beep_eq()                     
                j = j + 1
        
            old_ids =[]
            for i in feed:
                old_ids.append(i['id'])

        # suppose success with USGS
        USGS = True
        url="https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/4.5_week.geojson"
       # url="https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/1.0_week.geojson"
        try:
            conn = urllib.request.urlopen(url)
        except Exception:
            USGS = False 
            #self.root.after(60000, self.update_clock)
            pass
        
        if(USGS):
            try:
                data = conn.read()
                #print(' new call to USGS ',cc)
                cc = (cc+1)%10
            except Exception: 
                USGS = False            
            
            if(USGS):
                try:       
                    with open('USGS_data.txt','wb') as localFile:
                        localFile.write(data)
                        localFile.close()
                except Exception: 
                    USGS = False                

        with open('USGS_data.txt') as data_file:    
             geodata = json.load(data_file)
        
        list=range(0,geodata['metadata']['count']-1)
        #print(geodata['metadata']['count'])
       
        if(USGS):
            if( not GFZ):
                j=0
                txt1=[]
                txt2=[] 
            for i in list:
                idnow=geodata['features'][i]['id']
                #print(idnow)

                timestamp = geodata['features'][i]['properties']['time']/1000
                dt_obj = datetime.utcfromtimestamp(timestamp)
                date_str = dt_obj.strftime("%Y-%m-%d %H:%M:%S")
                #print(date_str)  
            
                txt1.append('USGS  '+geodata['features'][i]['properties']['title'][0:5]+" "+geodata['features'][i]['properties']['title'][7:50])
                txt2.append(date_str+  \
                    " \t"+str( '{0:.2f}'.format(    geodata['features'][i]['geometry']['coordinates'][1]  )   )+ \
                    " \t"+str(    '{0:.2f}'.format(   geodata['features'][i]['geometry']['coordinates'][0]   )   )+ \
                    " \t"+str(   '{0:.0f}'.format(   geodata['features'][i]['geometry']['coordinates'][2]   )   )+" km" )
                mag =  str(      geodata['features'][i]['properties']['title'])[2:5]         
                if (mag > "4.9" and not idnow in old_geos and not ( k == 1 ) ):
                     beep_eq()           
                j = j + 1
            
            old_geos=[]
            for i in list:
                old_geos.append(geodata['features'][i]['id'])
                               
               
 ######################################
        EE = True
        if(version == '1.1'):
             get_EE_eqks()
        elif(version == '1.2'):
             get_EE_eqks_new()
        #print(EE)
        if(EE  and  len(eetx1) > 0 ):
            if(not GFZ and not USGS):
                j=0
                txt1=[]
                txt2=[]
            list = range(0,len(eetx1)-1)
            for i in list:
                txt1.append(eetx1[i])
                txt2.append(eetx2[i]) 
                         

# make a unique structure of the texts
        zipped = zip(txt1,txt2)
        toprint = sorted(zipped,key=lambda x: x[1], reverse=True)

 # scrivo i dati         
        self.T1.delete('1.0','end')
        nn = range(0,j-1)
        s1 = toprint[0][1][0:19]        

        FMT = '%Y-%m-%d %H:%M:%S' 
        for i in nn:
            s2 = toprint[i][1][0:19]

            tdelta = datetime.strptime(s1, FMT) - datetime.strptime(s2, FMT)
      #      print (" tdelta ",tdelta.seconds) 
           
            if(tdelta.seconds > 120):
                  self.T1.insert('end', "  \n")
 
            if("GFZ" == toprint[i][0][0:3]  and toprint[i][0][8:11] >"4.9" ) :
                self.T1.insert('end',toprint[i][0]+"\t\t\t\t\t\t\t"+toprint[i][1]+"\n",'gfz_c')       
            elif ("USG" == toprint[i][0][0:3]   and toprint[i][0][8:11] > "4.9"    ):
                self.T1.insert('end',toprint[i][0]+"\t\t\t\t\t\t\t"+toprint[i][1]+"\n",'usgs_c')
            elif ("ING" == toprint[i][0][0:3]   and toprint[i][0][8:11] > "4.9" and toprint[i][0][8:11] <= "9.9"   ):
                self.T1.insert('end',toprint[i][0]+"\t\t\t\t\t\t\t"+toprint[i][1]+"\n",'ingv_c')
            else:
                self.T1.insert('end',toprint[i][0]+"\t\t\t\t\t\t\t"+toprint[i][1]+"\n")
            s1 = s2       
        # marco che il primo passaggio e-  avvenuto (per non suonare alla partenza)
        k=2

        now = time.strftime("%H:%M:%S")
        self.tempo.configure(text="                 >>>  GET   EARQUAKES  <<<         last update: "+now)            
            
        # re/entrant after n milliseconds
        self.root.after(60000, self.update_clock)

#app=App()


def main(argv):
   global version, eeurl
#   version = "1.1"
   version = "1.2"

#   eeurl="http://early-est.int.ingv.it/hypolist.html"
   eeurl="http://early-est.rm.ingv.it/hypolist.html"

#   eeurl="http://early-est-old.int.ingv.it/hypolist.html"
#   eeurl="http://early-est-dev.int.ingv.it:8080/hypolist.html"

   try:
      opts, args = getopt.getopt(argv,"hv:u:",["version=","ee-url="])
   except getopt.GetoptError:
      print( 'get_EE_GFZ_USGS [-v <version>] [-u <ee_url>]') 
      sys.exit(2)
   for opt, arg in opts:
      if opt == '-h':
         print('get_EE_GFZ_USGS -v <version> -u <ee_url>') 
         sys.exit()
      elif opt in ("-v", "--version"):
         version = arg
      elif opt in ("-u", "--ee_url"):
         eeurl = arg


   print( 'version is :', version)
   print( 'ee_url is :', eeurl)
   app=App()



if __name__ == "__main__":
   main(sys.argv[1:])

#############################################################
###################################################################################
########################################################################################
