#!/bin/bash
. /opt/CATenv/bin/activate
cd /opt/geteqk/get_eqk_py3  
nn=$(ps aux | grep -c get_EE_GFZ_USGS.py)
echo $nn
if [[ $nn < "2" ]] ; then
  python3 /opt/geteqk/get_eqk_py3/get_EE_GFZ_USGS.py
fi
