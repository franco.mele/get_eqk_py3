#import time 
from datetime import datetime
from obspy import UTCDateTime                
#  USGS  M 1.5 - 29km ENE of Bridgeport, California  2017-03-11 14:59:20    -118.9407 38.3942 7             
#  1489240760980
                
timestamp = 1489240760
dt_obj = datetime.utcfromtimestamp(timestamp)
date_str = dt_obj.strftime("%Y-%m-%d %H:%M:%S")
print(date_str)
