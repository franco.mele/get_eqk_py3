il programma interroga le pagine web di GFZ, USGS e Early-est per
mostrare le localizzazioni in tempo reale. 

I file utili sono:

get_EE_GFZ_USGS.py              effettivo programma in python 



start_get_EE_GFZ_USGS.bat       lancia il programma dal sistema Windows del
                                turnista CAT dove ho creato un environment
                                di nome CONDA_35 (python 3.5) che va attivato
                                prima di lanciare il programma.

                                
launch_get_EE_GFZ_USGS.sh       lancia il programma sulla macchina Linux del
                                turnista CAT, in un envirnoment python 3.5 
                                di nome CATenv.
                                

launch_get_EE_GFZ_USGS_osx.sh   lancia il programma su Mac OSX in environment             Conda                                
                                
                                
                                
                                

**INSTALLAZIONE**

Al sito:



https://docs.conda.io/en/latest/miniconda.html



Uso l’installer: di python 3.7.  64-bit  pkg



Dopo l'installazione,

aperto un terminale eseguo:



*$ conda create --name geteqk*



Il comando crea un environment di nome geteqk.

La risposta del sistema è:


```
Collecting package metadata (current_repodata.json): done
Solving environment: done
## Package Plan ##
  environment location: /opt/miniconda3/envs/geteqk
Proceed ([y]/n)? y
```


A cui rispondo “y”



IL sistema risponde con:


```
Preparing transaction: done
Verifying transaction: done
Executing transaction: done
#
# To activate this environment, use
#
#     $ conda activate geteqk
#
# To deactivate an active environment, use
#
#     $ conda deactivate
(base) MacBook-Pro$ 
```


Quindi eseguo il comando che attiva l'environment:

*$ conda activate geteqk*

geteqk è un nome arbitrario

Da questo momento in poi tutte le installazioni modificano solo questo environment e non influiscono sul resto del computer.

Devo installare delle librerie (rispondendo “y” alle domande):

*$ conda install feedparser*

*$ conda install pillow*

*$ conda install numpy*

*$ conda install -c conda-forge obspy*

*$ conda install -c anaconda beautifulsoup4*


Nella home directory copi il progetto git:

*$ cd*

*$ git clone https://gitlab.rm.ingv.it/franco.mele/get_eqk_py3.git*

*$ cd get_eqk_py3*

*$ python get_EE_GFZ_USGS.py*





E qui l’applicativo parte.

Good luck



P.S.



se nello stesso terminale eseguo ora il comando

*$ conda deactivate*

e poi eseguo python ritrovo la versione 2.7

Per attivare la 3.7 devo rieseguire:

*$ conda activate geteqk*

In alternativa se usi il comando

*$  . launch_get_EE_GFZ_USGS_osx.sh*

automaticamente esegue l'attivazione dell'environment conda e chiama il pyton 3.7.


**Aggiungo una nota su quale EE stiamo guardando**

Nella directory get_eqk_py3 se esegui

*$ python get_EE_GFZ_USGS.py*

automaticamente viene interrogata la pagina
early-est.rm.ingv.it. (versione 1.2.4)

Se usi il command file 
*$ . launch_get_EE_GFZ_USGS_osx.sh*

ora l'ho configurato per interrogare la stessa pagina esterna early-est.rm.ingv.it.

Se vuoi vedere la pagina interna in VPN devi scommentare una riga all'interno del commandfile e commentare l'altra.
Purtroppo nelle due versioni la lista delle localizzazioni ha numero di colonne e disposizione diverse.
  f.
                                ``
