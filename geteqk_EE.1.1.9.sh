#!/bin/bash
conda activate geteqk
cd ~/get_eqk_py3
nn=$(ps aux | grep -c get_EE_GFZ_USGS.py)
echo $nn
if [[ $nn < "2" ]] ; then
 python ~/get_eqk_py3/get_EE_GFZ_USGS.py -v 1.1 -u http://early-est-old.int.ingv.it/hypolist.html
#  python ~/get_eqk_py3/get_EE_GFZ_USGS.py -v 1.2 -u http://early-est.rm.ingv.it/hypolist.html
fi
